import dataProcessing as dp
import tensorflow as tf
import numpy as np

filename = '2017.3.10.xls'
alldata = dp.dtprocess(filename)  # 此时为（时间片，传感器，轴，频率）
k_sensors = dp.k_sensors
time_intervals = alldata[0]
channel = 1
layer1_deeps = 64
layer1_patchx = 3
layer1_patchy = 2
layer2_patchx = 1
layer2_patchy = 3
layer2_deeps = 64
layer3_patchx = 1
layer3_patchy = 2
layer3_deeps = 64

Mlayer1_deeps = 64
Mlayer1_patchx = k_sensors
Mlayer1_patchy = 2
Mlayer2_patchx = 1
Mlayer2_patchy = 3
Mlayer2_deeps = 64
Mlayer3_patchx = 1
Mlayer3_patchy = 2
Mlayer3_deeps = 64
epsilon = 0.001

GRU1_out_node = 64
GRU2_out_node = 64

graph = tf.Graph()
with graph.as_default():
    # input data
    tf_all_dataset = tf.constant(alldata[:, :, :, :, None])   # 这是一个5维的数据 （时间片，传感器，轴，频率，1）

    # Variables
    layer1_weights = tf.Variable(tf.truncated_normal(
        [layer1_patchx, layer1_patchy, channel, layer1_deeps], stddev=0.1))
    layer1_BNscale = tf.Variable(tf.ones([layer1_deeps]))
    layer1_BNoffset = tf.Variable(tf.zeros([layer1_deeps]))

    layer2_weights = tf.Variable(tf.truncated_normal(
        [layer2_patchx, layer2_patchy, layer1_deeps, layer2_deeps], stddev=0.1))
    layer2_BNscale = tf.Variable(tf.ones([layer2_deeps]))
    layer2_BNoffset = tf.Variable(tf.zeros([layer2_deeps]))

    layer3_weights = tf.Variable(tf.truncated_normal(
        [layer3_patchx, layer3_patchy, layer2_deeps, layer3_deeps], stddev=0.1))
    layer3_BNscale = tf.Variable(tf.ones([layer3_deeps]))
    layer3_BNoffset = tf.Variable(tf.zeros([layer3_deeps]))

    #Mergelayers
    Mlayer1_weights = tf.Variable(tf.truncated_normal(
        [Mlayer1_patchx, Mlayer1_patchy, channel, Mlayer1_deeps], stddev=0.1))
    Mlayer1_BNscale = tf.Variable(tf.ones([Mlayer1_deeps]))
    Mlayer1_BNoffset = tf.Variable(tf.zeros([Mlayer1_deeps]))

    Mlayer2_weights = tf.Variable(tf.truncated_normal(
        [Mlayer2_patchx, Mlayer2_patchy, Mlayer1_deeps, Mlayer2_deeps], stddev=0.1))
    Mlayer2_BNscale = tf.Variable(tf.ones([Mlayer2_deeps]))
    Mlayer2_BNoffset = tf.Variable(tf.zeros([Mlayer2_deeps]))

    Mlayer3_weights = tf.Variable(tf.truncated_normal(
        [Mlayer3_patchx, Mlayer3_patchy, Mlayer2_deeps, Mlayer3_deeps], stddev=0.1))
    Mlayer3_BNscale = tf.Variable(tf.ones([Mlayer3_deeps]))
    Mlayer3_BNoffset = tf.Variable(tf.zeros([Mlayer3_deeps]))



    # Individual Convolutional model
    def sepCNNmodel(data):
        # 此处的data为某一个时间片上不同的设备的data，
        # 传入的是四维的数据（传感器，轴，频率，1）
        conv = tf.nn.conv2d(data, layer1_weights, [1, 1, 1, 1], padding="VALID")  # 其中的数组分别是 data 的四个维度的stride
        #  Branch Nomalization
        BN_mean, BN_var = tf.nn.moments(conv, list(range(len(conv.get_shape()) - 1)))
        BN_conv = tf.nn.batch_normalization(conv, BN_mean, BN_var,
                                            layer1_BNoffset, layer1_BNscale, epsilon)
        hidden = tf.nn.relu(BN_conv)
        conv = tf.nn.conv2d(hidden, layer2_weights, [1, 1, 1, 1], padding="VALID")
        BN_mean, BN_var = tf.nn.moments(conv, list(range(len(conv.get_shape()) - 1)))
        BN_conv = tf.nn.batch_normalization(conv, BN_mean, BN_var,
                                            layer2_BNoffset, layer2_BNscale, epsilon)
        hidden = tf.nn.relu(BN_conv)
        conv = tf.nn.conv2d(hidden, layer3_weights, [1, 1, 1, 1], padding="VALID")
        BN_mean, BN_var = tf.nn.moments(conv, list(range(len(conv.get_shape()) - 1)))
        BN_conv = tf.nn.batch_normalization(conv, BN_mean, BN_var,
                                            layer3_BNoffset, layer3_BNscale, epsilon)
        hidden = tf.nn.relu(BN_conv)
        return hidden  # 返回的也是一个四维的数据

    # Flatten & Concatenation
    def mergeCNN(hidden):
        #  hidden是sepCNNmodel返回的四维的数据
        #  k, x, y, d = hidden.shape
        #  merge = hidden[0].reshape((-1,))[None, :]
        #  for i in range(1, k):
        #    merge = np.concatenate((merge, hidden[i].reshape((-1,))[None, :]), axis=0)
        #  merge = np.array(list(map(lambda x: x.reshape((-1,)), hidden)))
        merge = tf.reshape(hidden, (hidden._shape_as_list()[0], -1))
        return merge  # 这是合并之后的一个二维数据

    # Merge Convolutional & Flatten
    def megCNNnodel(merge):
        # merge是mergeCNN返回的二维的数据
        data = merge[None, :, :, None]  # 将二维数据变成四维数据
        conv = tf.nn.conv2d(data, Mlayer1_weights, [1, 1, 1, 1], padding="VALID")  # 其中的数组分别是 data 的四个维度的stride
        BN_mean, BN_var = tf.nn.moments(conv, list(range(len(conv.get_shape()) - 1)))
        BN_conv = tf.nn.batch_normalization(conv, BN_mean, BN_var,
                                            Mlayer1_BNoffset, Mlayer1_BNscale, epsilon)
        hidden = tf.nn.relu(BN_conv)
        conv = tf.nn.conv2d(hidden, Mlayer2_weights, [1, 1, 1, 1], padding="VALID")
        BN_mean, BN_var = tf.nn.moments(conv, list(range(len(conv.get_shape()) - 1)))
        BN_conv = tf.nn.batch_normalization(conv, BN_mean, BN_var,
                                            Mlayer2_BNoffset, Mlayer2_BNscale, epsilon)
        hidden = tf.nn.relu(BN_conv)
        conv = tf.nn.conv2d(hidden, Mlayer3_weights, [1, 1, 1, 1], padding="VALID")
        BN_mean, BN_var = tf.nn.moments(conv, list(range(len(conv.get_shape()) - 1)))
        BN_conv = tf.nn.batch_normalization(conv, BN_mean, BN_var,
                                            Mlayer3_BNoffset, Mlayer3_BNscale, epsilon)
        hidden = tf.nn.relu(BN_conv)
        flat = tf.reshape(hidden, (-1,))
        return flat  # 返回的是展平后的一维数据

    def before_GRU(data):
        # 此处的data为某一个时间片上不同的设备的data，
        # 传入的是四维的数据（传感器，轴，频率，1）
        hidden = sepCNNmodel(data)
        merge = mergeCNN(hidden)
        flat = megCNNnodel(merge)
        return flat  # 返回的是展平后的一维数据


    def get_GRU_data(tf_all_dataset):
        #  GRU_data = before_GRU(tf_all_dataset[0])[:, None]
        GRU_data = tf.map_fn(before_GRU, tf_all_dataset)
        return GRU_data  # 返回的是二维数据（时间片， 合并数据）


    def batch_norm(x, name_scope, training, epsilon=epsilon, decay=0.999):
        '''Assume 2d [batch, values] tensor'''
        # 第一个参数是需要BN的对象， 第二个是命名空间， 第三个bool值表示是否在训练
        with tf.variable_scope(name_scope):
            size = x.get_shape().as_list()[1]

            scale = tf.get_variable('scale', [size], initializer=tf.constant_initializer(0.1))
            offset = tf.get_variable('offset', [size])

            pop_mean = tf.get_variable('pop_mean', [size], initializer=tf.zeros_initializer, trainable=False)
            pop_var = tf.get_variable('pop_var', [size], initializer=tf.ones_initializer, trainable=False)
            batch_mean, batch_var = tf.nn.moments(x, list(range(len(x.get_shape()) - 1)))

            train_mean_op = tf.assign(pop_mean, pop_mean * decay + batch_mean * (1 - decay))
            train_var_op = tf.assign(pop_var, pop_var * decay + batch_var * (1 - decay))

            def batch_statistics():
                with tf.control_dependencies([train_mean_op, train_var_op]):
                    return tf.nn.batch_normalization(x, batch_mean, batch_var, offset, scale, epsilon)

            def population_statistics():
                return tf.nn.batch_normalization(x, pop_mean, pop_var, offset, scale, epsilon)

            return tf.cond(training, batch_statistics, population_statistics)  # 类似于条件函数


    def GRU_cell(name_scope, h_t1, x_t, training):
        with tf.name_scope(name_scope):
            GRU_in_node = x_t.get_shape().as_list()[1]  # x和h是一个二维数据
            GRU_out_node = h_t1.get_shape().as_list()[1]
            # Zt compute
            zt_w = tf.get_variable('zt_w', [GRU_in_node, GRU_out_node],
                                   initializer=tf.truncated_normal_initializer(-0.1, 0.1))
            zt_r = tf.get_variable('zt_r', [GRU_out_node, GRU_out_node],
                                   initializer=tf.truncated_normal_initializer(-0.1, 0.1))
            zt_b = tf.get_variable('zt_b', [1, GRU_out_node],
                                   initializer=tf.zeros_initializer)
            # Rt compute
            rt_w = tf.get_variable('rt_w', [GRU_in_node, GRU_out_node],
                                   initializer=tf.truncated_normal_initializer(-0.1, 0.1))
            rt_r = tf.get_variable('rt_r', [GRU_out_node, GRU_out_node],
                                   initializer=tf.truncated_normal_initializer(-0.1, 0.1))
            rt_b = tf.get_variable('rt_b', [1, GRU_out_node],
                                   initializer=tf.zeros_initializer)
            # Yt_compute
            yt_w = tf.get_variable('yt_w', [GRU_in_node, GRU_out_node],
                                   initializer=tf.truncated_normal_initializer(-0.1, 0.1))
            yt_r = tf.get_variable('yt_r', [GRU_out_node, GRU_out_node],
                                   initializer=tf.truncated_normal_initializer(-0.1, 0.1))
            yt_b = tf.get_variable('yt_b', [1, GRU_out_node],
                                   initializer=tf.zeros_initializer)
            zt = tf.sigmoid(batch_norm(tf.multiply(x_t, zt_w), "zt_w", training)
                            + batch_norm(tf.multiply(h_t1, zt_r), "zt_r", training) + zt_b)
            rt = tf.sigmoid(batch_norm(tf.multiply(x_t, rt_w), "rt_w", training)
                            + batch_norm(tf.multiply(h_t1, rt_r), "rt_r", training) + rt_b)
            yt = tf.tanh(batch_norm(tf.multiply(x_t, yt_w), "yt_w", training)
                         + batch_norm(tf.multiply(h_t1 * rt, yt_r), "yt_r", training) + yt_b)
            ht = zt * yt + (1 - zt) * h_t1
            return ht

    GRU_data = get_GRU_data(tf_all_dataset)  # （时间片， 合并数据）
    GRU1_train_data = GRU_data[:, None, :]  # 三维数据（时间片，1， 合并数据）
    # # Zt compute
    # zt_w  = tf.Variable(tf.truncated_normal([GRU_in_node, GRU_out_node], -0.1, 0.1))
    # zt_r = tf.Variable(tf.truncated_normal([GRU_out_node, GRU_out_node], -0.1, 0.1))
    # zt_b = tf.Variable(tf.zeros([1, GRU_out_node]))
    # # Rt compute
    # rt_w = tf.Variable(tf.truncated_normal([GRU_in_node, GRU_out_node], -0.1, 0.1))
    # rt_r = tf.Variable(tf.truncated_normal([GRU_out_node, GRU_out_node], -0.1, 0.1))
    # rt_b = tf.Variable(tf.zeros([1, GRU_out_node]))
    # # Yt_compute
    # yt_w = tf.Variable(tf.truncated_normal([GRU_in_node, GRU_out_node], -0.1, 0.1))
    # yt_r = tf.Variable(tf.truncated_normal([GRU_out_node, GRU_out_node], -0.1, 0.1))
    # yt_b = tf.Variable(tf.zeros([1, GRU_out_node]))
    # initial_variables
    saved_h1 = tf.Variable(tf.zeros([1, GRU1_out_node]), trainable=False)
    saved_h2 = tf.Variable(tf.zeros([1, GRU2_out_node]), trainable=False)


    # 第一层GRU
    h_outs1 = list()
    ht1 = saved_h1
    for x in GRU1_train_data:
        ht1 = GRU_cell("G1", ht1, x, True)
        h_outs1.append(ht1)
    saved_h1.assign(ht1)  # 保留计算出的数据
    GRU2_train_data = tf.nn.dropout(tf.concat(h_outs1, 0), 0.5)  # 三维数据（时间片, 1, 64）

    # 第二层GRU
    h_outs2 = list()
    ht2 = saved_h2
    for x in GRU2_train_data:
        ht2 = GRU_cell("G2", ht2, x, True)
        h_outs2.append(ht2)
    saved_h2.assign(ht2)  # 保留计算出的数据
    GRU2_out = tf.concat(h_outs2, 0)  # 三维数据（时间片, 1, 64）

    y_hat = tf.reduce_mean(tf.reduce_mean(GRU2_out, 0), 0)


    







