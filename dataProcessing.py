import xlrd
import numpy as np
k_sensors = 3
axis = 3


def readdata(name):
    table = xlrd.open_workbook(name).sheets()[0]
    rows = table.nrows
    alldata = np.array([table.row_values(x)[1:] for x in range(1, rows)], ) \
        .astype('float64').transpose()
    print(alldata[0:axis].shape)
    print(alldata[0:axis][:, 0])
    alldata = alldata.reshape((k_sensors, axis, -1))  # 分别表示sensor，三轴，时序数据
    print(alldata.shape)
    return alldata


def t2f(array):
    splength = 100  # 取样长度。这里和采样频率一样，保证时间间隔为１秒
    time_intervals = len(array) // splength
    out = np.fft.fft(array[0:splength])[None, :]
    for j in range(1, time_intervals):
        i = j * splength
        out = np.concatenate((out, np.fft.fft(array[i:i+splength])[None, :]), axis=0)
    return out  # 每一行表示单个时间片的傅里叶变换值


def two2three(_2darray):
    # row, col = _2darray.shape
    # out1 = t2f(_2darray[0])[None, :]
    # for i in range(1, row):
    #     out1 = np.concatenate((out1, t2f(_2darray[i])[None, :]), axis=0)
    out1 = np.array(list(map(lambda x: t2f(x), _2darray)))
    # 此时为 (轴，时间片， 频率)
    return np.swapaxes(out1, 0, 1)  # 此时为(时间片，轴， 频率)


def three2four(alldata):
    # 增加一个传感器轴，返回的是（传感器，时间片，轴，频率）的四维数据
    # k = alldata.shape[0]
    # out = two2three(alldata[0])[None, :]
    # for i in range(1, k):
    #     out = np.concatenate((out, two2three(alldata[i])[None, :]), axis=0)
    out = np.array(list(map(lambda x: two2three(x), alldata)))
    return out


def dtprocess(name):
    alldt = readdata(name)
    final = np.swapaxes(three2four(alldt), 0, 1)
    print(final.shape)
    return final  # 此时为（时间片，传感器，轴，频率）

if __name__ == "__main__":
    name = "2017.3.10.xls"
    dtprocess(name)






